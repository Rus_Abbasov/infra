# Клонирование репозитория

Для клонирования репозитория необходимо выполнить команду: 
```
git clone https://gitlab.com/Rus_Abbasov/infra.git
```

# Инструкция по работе

**Вся инфраструктура работает через облачный провайдер [Yandex Cloud](https://cloud.yandex.ru/ru/).**

Необходимо зарегистрировать **домен** (напр. на веб-сайте [NIC](https://www.nic.ru)), задав ему DNS-сервера: **ns1.yandexcloud.net** и **ns2.yandexcloud.net**.

В качестве системы сбора логов выбрана **облачная система сбора логов Sumo Logic**. Необходимо зарегистрировать аккаунт на сайте [Sumo Logic](https://www.sumologic.com/), затем необходимо скопировать **Installation Token** (Add Collection\SUMOLOGIC_INSTALLATION_TOKEN).

### Изменение конфигурации 

После клонирования репозитория необходимо перейти в директорию **./infra/s3**. 

Далее в файле [variables.tf](https://gitlab.com/Rus_Abbasov/infra/-/blob/master/s3/variables.tf?ref_type=heads) необходимо прописать значения переменных: **token**, **cloud_id**, **folder_id** (установка [YC CLI](https://cloud.yandex.ru/ru/docs/cli/operations/install-cli) и [его конфигурация](https://cloud.yandex.ru/ru/docs/cli/operations/profile/profile-create)).

Затем необходимо перейти в директорию **./infra/main**.

Далее в файле [variables.tf](https://gitlab.com/Rus_Abbasov/infra/-/blob/master/main/variables.tf?ref_type=heads) необходимо прописать значения переменных: **token**, **cloud_id**, **folder_id** (установка [YC CLI](https://cloud.yandex.ru/ru/docs/cli/operations/install-cli) и [его конфигурация](https://cloud.yandex.ru/ru/docs/cli/operations/profile/profile-create)), а также прописать ранее созданный домен в переменную: **domain(указав домен с точкой в конце)**.

Затем переходим в директорию **./infra/main/ansible/vars**.

Необходимо из репозитория [diploma](https://gitlab.com/Rus_Abbasov/diploma) скопировать **Registration token GitLab раннера**(Settings\CI/CD Settings\Runners\Project runners) и прописать его в файл [gitlab_runner_vars.yml](https://gitlab.com/Rus_Abbasov/infra/-/blob/master/main/ansible/vars/gitlab_runner_vars.yml?ref_type=heads) (**token: "..."**)

Затем переходим в директорию **./infra/main/ansible/roles/sumologic_agent/defaults**.

Для корректной работы Sumo Logic Agent необходимо прописать в файл **main.yml** ранее скопированный SUMOLOGIC_INSTALLATION_TOKEN (**sumologic_installation_token: ...**)

### Создание GitLab переменных

В репозитории [diploma](https://gitlab.com/Rus_Abbasov/diploma) необходимо создать несколько переменных(Settings\CI/CD Settings\Variables\Add variable):

- CI_REGISTRY_IMAGE (название образа для приложения)
- CI_REGISTRY_PASSWORD (Access Token для входа в dockerhub)
- CI_REGISTRY_USER (логин для входа в dockerhub)
- PASSPHRASE (пароль от ключа ssh, необходимого для работы ansible-playbook)

### Создание инфраструктуры

В проекте присутствует **хранение состояния конфигурации с помощью бакета s3**.

Прежде всего необходимо создать **сервисный аккаунт с ролью editor**, **получить статический ключ доступа** и **создать бакет**.

Для этого необходимо перейти в директорию **./infra/s3** и выполнить команды:
```
terraform init
```
а затем
```
terraform apply -auto-approve
```

После завершения необходимо вывести **идентификатор ключа** и **секретный ключ**, выполнив команды:
```
terraform output -raw yandex_iam_service_account_static_access_key
```
и
```
terraform output -raw yandex_iam_service_account_static_secret_key
```

Затем нужно добавить в переменные окружения **идентификатор ключа** и **секретный ключ**, полученные ранее, выполнив команды:
```
export ACCESS_KEY="<идентификатор_ключа>"
```
а также
```
export SECRET_KEY="<секретный_ключ>"
```

Далее необходимо перейти в директорию **./infra/main** и выполнить команды:
```
terraform init -backend-config="access_key=$ACCESS_KEY" -backend-config="secret_key=$SECRET_KEY"
```
а затем
```
terraform apply -auto-approve
```

После завершения будут выведены: **IP-адрес сервера с продакшн окружением**, **IP-адрес сервера с тестовым окружением**, **IP-адрес сервера с GitLab раннером**, **IP-адрес сервера c Prometheus** и **IP-адрес сервера с Grafana**.

По итогу будут созданы: **инфраструктура с сохранением состояний конфигурации**, состоящая из **балансировщика нагрузки (URL: http://www.домен:8080)**, **продакшн сервера, который будет направлен на балансировщик нагрузки (URL: http://www.prod.домен:8080)**, **сервера с тестовым окружением (URL: http://www.test.домен:8080)**, **сервера с GitLab раннером (URL: http://www.ci.домен)**, **сервера с мониторингом Prometheus (URL: http://www.monitoring.домен:9090)** и **сервера с Grafana (URL: http://www.grafana.домен:3000)**. На продакшн сервере будут установлены **docker**, **docker-compose**, **node-exporter** и **sumo-logic-agent**; на сервере с тестовым окружением будут установлены **docker**, **docker-compose**, **node-exporter** и **sumo-logic-agent**; на сервере с GitLab раннером будут установлены **docker**, **docker-compose**, **ansible**, **gitlab-runner**, **node-exporter** и **sumo-logic-agent**; на сервере с Prometheus будут установлены **prometheus**, **blackbox_exporter**, **node-exporter** и **sumo-logic-agent**; на сервере с Grafana будут установлены **grafana**, **node-exporter** и **sumo-logic-agent**.

### Импорт готового Dashboard в Grafana

Готовый Dashboard будет автоматически импортирован в Grafana (URL: http://www.grafana.домен:3000/dashboards)

Логин и пароль для входа в веб-интерфейс Grafana по умолчанию - admin

### Облачная система сбора логов Sumo Logic

Все сервисы, с которых будут собираться логи автоматически добавляются в веб-интерфейс Sumo Logic.

Просмотр собранных логов с сервисов доступен в [веб-интерфейсе Sumo Logic](https://service.sumologic.com/ui/#/home) в разделе **Log Search**.
