terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"

  backend "s3" {
    endpoints = {
      s3 = "https://storage.yandexcloud.net"
    }
    bucket = "diploma-bucket-main"
    region = "ru-central1"
    key    = "diploma-bucket.tfstate"
        
    skip_region_validation      = true
    skip_credentials_validation = true
    skip_requesting_account_id  = true
    skip_s3_checksum            = true
  }
}

provider "yandex" {
  token     = var.token
  cloud_id  = var.cloud_id
  folder_id = var.folder_id
  zone      = var.zone
}

data "yandex_compute_image" "my-ubuntu-2004-1" {
  family = "ubuntu-2004-lts"
}

resource "yandex_compute_instance" "prod" {
  name        = "diploma-vm-prod"
  hostname    = "prod"
  platform_id = "standard-v1"
  zone        = var.zone

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.my-ubuntu-2004-1.id
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.diploma-sn.id
    nat = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file(var.ssh_key_public)}"
  }

  provisioner "remote-exec" {
    inline = ["sudo apt -y install python3"]

    connection {
      host        = yandex_compute_instance.prod.network_interface.0.nat_ip_address
      type        = "ssh"
      user        = "ubuntu"
      agent       = true
      }
  }

  provisioner "local-exec" {
    command = "ansible-playbook -u ubuntu -i '${yandex_compute_instance.prod.network_interface.0.nat_ip_address},' --private-key ${var.ssh_key_private} -T 300 ansible/infra_setup.yml --ssh-common-args='-o StrictHostKeyChecking=no'"
  }
}
    
resource "yandex_compute_instance" "test" {
  name        = "diploma-vm-test"
  hostname    = "test"
  platform_id = "standard-v1"
  zone        = var.zone

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.my-ubuntu-2004-1.id
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.diploma-sn.id
    nat = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file(var.ssh_key_public)}"
  }
  
  provisioner "remote-exec" {
    inline = ["sudo apt -y install python3"]

    connection {
      host        = yandex_compute_instance.test.network_interface.0.nat_ip_address
      type        = "ssh"
      user        = "ubuntu"
      agent       = true
      }
  }

  provisioner "local-exec" {
    command = "ansible-playbook -u ubuntu -i '${yandex_compute_instance.test.network_interface.0.nat_ip_address},' --private-key ${var.ssh_key_private} -T 300 ansible/infra_setup.yml --ssh-common-args='-o StrictHostKeyChecking=no'" 
  }
}

resource "yandex_compute_instance" "gitlab-runner" {
  name        = "diploma-vm-gitlab-runner"
  hostname    = "gitlab-runner"
  platform_id = "standard-v1"
  zone        = var.zone

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.my-ubuntu-2004-1.id
      size     = 8
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.diploma-sn.id
    nat = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file(var.ssh_key_public)}"
  }

  provisioner "remote-exec" {
    inline = ["sudo apt -y install python3"]

    connection {
      host        = yandex_compute_instance.gitlab-runner.network_interface.0.nat_ip_address
      type        = "ssh"
      user        = "ubuntu"
      agent       = true
      }
  }

  provisioner "local-exec" {
    command = "ansible-playbook -u ubuntu -i '${yandex_compute_instance.gitlab-runner.network_interface.0.nat_ip_address},' --private-key ${var.ssh_key_private} -T 300 ansible/gitlab_runner_setup.yml --ssh-common-args='-o StrictHostKeyChecking=no'"
  }
}

resource "yandex_compute_instance" "monitoring" {
  name        = "diploma-vm-monitoring"
  hostname    = "monitoring"
  platform_id = "standard-v1"
  zone        = var.zone

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.my-ubuntu-2004-1.id
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.diploma-sn.id
    nat = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file(var.ssh_key_public)}"
  }

  provisioner "remote-exec" {
    inline = ["sudo apt -y install python3"]

    connection {
      host        = yandex_compute_instance.monitoring.network_interface.0.nat_ip_address
      type        = "ssh"
      user        = "ubuntu"
      agent       = true
      }
  }

  provisioner "local-exec" {
    command = "ansible-playbook -u ubuntu -i '${yandex_compute_instance.monitoring.network_interface.0.nat_ip_address},' --private-key ${var.ssh_key_private} -T 300 ansible/monitoring_setup.yml --ssh-common-args='-o StrictHostKeyChecking=no'"
  }
}

resource "yandex_compute_instance" "grafana" {
  name        = "diploma-vm-grafana"
  hostname    = "grafana"
  platform_id = "standard-v1"
  zone        = var.zone

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.my-ubuntu-2004-1.id
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.diploma-sn.id
    nat = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file(var.ssh_key_public)}"
  }

  provisioner "remote-exec" {
    inline = ["sudo apt -y install python3"]

    connection {
      host        = yandex_compute_instance.grafana.network_interface.0.nat_ip_address
      type        = "ssh"
      user        = "ubuntu"
      agent       = true
      }
  }

  provisioner "local-exec" {
    command = "ansible-playbook -u ubuntu -i '${yandex_compute_instance.grafana.network_interface.0.nat_ip_address},' --private-key ${var.ssh_key_private} -T 300 ansible/grafana_setup.yml --ssh-common-args='-o StrictHostKeyChecking=no'"
  }
}

resource "yandex_vpc_network" "default" {
  name = "diploma-nw"
}

resource "yandex_vpc_subnet" "diploma-sn" {
  name           = "diploma-sn"
  zone           = var.zone
  network_id     = yandex_vpc_network.default.id
  v4_cidr_blocks = ["192.168.10.0/24"]
}

resource "yandex_lb_target_group" "diploma-lb-tg" {
  name      = "diploma-target-group"
  region_id = "ru-central1"

  target {
    subnet_id = "${yandex_vpc_subnet.diploma-sn.id}"
    address   = "${yandex_compute_instance.prod.network_interface.0.ip_address}"
  }
}

resource "yandex_lb_network_load_balancer" "lb" {
  name = "diploma-load-balancer"

  listener {
    name = "diploma-listener"
    port = 8080
    external_address_spec {
      ip_version = "ipv4"
    }
  }

  attached_target_group {
    target_group_id = yandex_lb_target_group.diploma-lb-tg.id

    healthcheck {
      name = "http"
      http_options {
        port = 8080
        path = "/"
      }
    }
  }
}

resource "yandex_dns_zone" "zone1" {
  name        = "diploma-zone1"
  zone        = var.domain
  public      = true
}

resource "yandex_dns_recordset" "lb" {
  zone_id = yandex_dns_zone.zone1.id
  name    = "www.${var.domain}"
  type    = "A"
  ttl     = 200
  data    = [[for s in yandex_lb_network_load_balancer.lb.listener: s.external_address_spec.*.address].0[0]]
}

resource "yandex_dns_recordset" "prod" {
  zone_id = yandex_dns_zone.zone1.id
  name    = "www.prod.${var.domain}"
  type    = "A"
  ttl     = 200
  data    = [yandex_compute_instance.prod.network_interface.0.nat_ip_address]
}

resource "yandex_dns_recordset" "test" {
  zone_id = yandex_dns_zone.zone1.id
  name    = "www.test.${var.domain}"
  type    = "A"
  ttl     = 200
  data    = [yandex_compute_instance.test.network_interface.0.nat_ip_address]
}

resource "yandex_dns_recordset" "gitlab-runner" {
  zone_id = yandex_dns_zone.zone1.id
  name    = "www.ci.${var.domain}"
  type    = "A"
  ttl     = 200
  data    = [yandex_compute_instance.gitlab-runner.network_interface.0.nat_ip_address]
}

resource "yandex_dns_recordset" "monitoring" {
  zone_id = yandex_dns_zone.zone1.id
  name    = "www.monitoring.${var.domain}"
  type    = "A"
  ttl     = 200
  data    = [yandex_compute_instance.monitoring.network_interface.0.nat_ip_address]
}

resource "yandex_dns_recordset" "grafana" {
  zone_id = yandex_dns_zone.zone1.id
  name    = "www.grafana.${var.domain}"
  type    = "A"
  ttl     = 200
  data    = [yandex_compute_instance.grafana.network_interface.0.nat_ip_address]
}

output "external_ip_address_diploma-vm-prod" {
  value = yandex_compute_instance.prod.network_interface.0.nat_ip_address
}

output "external_ip_address_diploma-vm-test" {
  value = yandex_compute_instance.test.network_interface.0.nat_ip_address
}

output "external_ip_address_gitlab-runner" {
  value = yandex_compute_instance.gitlab-runner.network_interface.0.nat_ip_address
}

output "external_ip_address_monitoring" {
  value = yandex_compute_instance.monitoring.network_interface.0.nat_ip_address
}

output "external_ip_address_grafana" {
  value = yandex_compute_instance.grafana.network_interface.0.nat_ip_address
}
