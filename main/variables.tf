variable "token" {
	default = "..."
}

variable "cloud_id" {
        default = "..."
}

variable "folder_id" {
        default = "..."
}

variable "zone" {
        default = "ru-central1-a"
}

variable "domain" {
        default = "..."
}

variable "ssh_key_private" {
        default = "~/.ssh/id_rsa"
}

variable "ssh_key_public" {
        default = "~/.ssh/id_rsa.pub"
}
